Rails.application.routes.draw do

  namespace :admin do
  root 'application#index'
    resources :users, :only => [:index]
    resources :categories, :except => [:show]
    resources :comments, :only => [:index]
    resources :comments, :only => [:index]
    resources :attendances,:only => [:index]
    resources :likes, :only => [:index]
  end

  devise_for :users
root 'home#index'

  resources :events do
    resources :comments
    resources :attendances, only: [:create]
    resources :likes, :only => [:create]
    resources :tags, :only => [:show]
  end

  resources :users, only: [:show]
  resources :categories, :only => [:show]
mount ActionCable.server => '/cable'
end
