# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(email: "test@test.com", password: "asdfasdf", password_confirmation: "asdfasdf", first_name: "regular", last_name: "user",
            username: "regular_user",organization_name: "regularUSER INFOG")
User.create!(email: "admintest@test.com", password: "asdfasdf", password_confirmation: "asdfasdf",first_name: "admin", last_name: "user",
             username: "admin_user",organization_name: "adminrUSER INFOG",admin: true)

10.times do
  Category.create(name: Faker::Book.genre, summary: Faker::Hipster.paragraph(3,true,4))
end
8.times do |n|
  Event.create(title: Faker::Book.title, description: Faker::Lorem.paragraph(n, false),start_date: DateTime.now,
               end_date:( DateTime.now +(n+2).days),
               location: Faker::Address.state, venue: Faker::Address.street_address,
                user_id: User.first.id,
                category_id: Category.find(n+1),
                seats: n)
  end


8.times do
  Tag.create(name: Faker::Superhero.power)
end

8.times do |e|
  Event.find(e+1).tags << Tag.find(e+1)
  Event.find(e+1).save

end