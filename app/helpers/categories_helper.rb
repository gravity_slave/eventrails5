module CategoriesHelper

  def active_category? cat

    active? "/categories/#{cat.id}"


  end
end
