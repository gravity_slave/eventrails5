module ApplicationHelper

  def full_title page_title
    default_title = "EventsBoard - Create Events to Socialize"
    page_title.empty?  ? default_title : "#{page_title} | #{default_title}"
  end

  def format_time event
    event.strftime("%A, %d %b %Y %l:%M %p")
  end

  def active? path

    if  current_page? path
        "active"
    else
      ""
    end
  end

  def active_user_id? user

    active? "/users/#{user.id}"


  end

def admins_only(&block)
  block.call if current_user.try(:admin?)
end

  def gravatar_for user
    hash = Digest::MD5.hexdigest(user.email.downcase)
    "https://www.gravatar.com/avatar/#{hash}"

  end

end
