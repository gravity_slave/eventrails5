class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  extend FriendlyId
  friendly_id :username, use: :slugged

  has_many :attended_events, through: :attendances
  has_many :attendances
  has_many :organized_events, class_name: "Event", :dependent => :destroy
  has_many :comments
  validates :username, presence: true, length:  { minimum: 6}
  def full_name
    "#{self.first_name} #{self.last_name}"
  end
end
