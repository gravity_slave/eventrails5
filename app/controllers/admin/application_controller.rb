class Admin::ApplicationController < ApplicationController
  before_action :authorize_admin?
  skip_after_action :verify_authorized

  def index
    @users =User.all
    @events = Event.all
    @categories = Category.all
    @comments = Comment.all
    @attendances = Attendance.all
    @likes =Like.all
  end

  private
  def authorize_admin?
    authenticate_user!
    unless current_user.admin?
      redirect_to root_path, :alert => "You mustbe an admin to to execute that"
    end
  end
end
