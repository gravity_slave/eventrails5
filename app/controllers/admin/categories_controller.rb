class Admin::CategoriesController < Admin::ApplicationController
before_action :set_category, :only =>  [:edit, :update, :destroy]
def index
  super
  @categories = Category.order(:name)
end

  def new
@category =Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path, :notice => 'Category has been created'
    else
      flash.now[:alert] = "Category hasn't been created"
      render 'new'
    end

  end

  def edit

  end

  def update
    if @category.update_attributes(category_params)
      redirect_to admin_categories_path, :notice => "Category has been updated"
    else
      flash.now[:alert] = "Category has not been updated"
      render 'edit'
    end

  end

  def destroy
 @category.destroy
    redirect_to admin_categories_path, :alert => "Category has been deleted"

  end

  private

def set_category
  @category = Category.friendly.find(params[:id])
end

def category_params
  params.require(:category).permit(:name, :summary)
end
end

