class EventsController < ApplicationController
  prepend_before_action :set_event, :only => [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:index, :show]

  def index
    if params[:query].present?
      @events = Event.search(params[:query])

    else
      @events = Event.recent_go_first.paginate(page: params[:page], per_page: 5)

    end
    @categories = Category.order(:name)



 authorize @events, :index?
  end


  def show
    authorize @event, :show?
    @comments = @event.comments.paginate(page: params[:page], per_page: 5)

    @comment = Comment.new
    @comment.event_id = @event.id


  end

  def new

    @event =Event.new


    authorize @event, :new?


  end

  def create
    @event =Event.new(event_params)
    @event.organizer =  current_user
    authorize @event, :create?


    if @event.save
    redirect_to @event, notice: "Event created!"
  else
    flash.now[:alert] = "Failed to create event :("
    render "new"

  end
  end

  def  edit
    authorize @event

  end

  def update
    authorize @event, :update?
    if @event.update_attributes(event_params)
      redirect_to @event, notice:  "You have updated event successfully"
    else
      flash.now[:alert] = "Event was nt updated"
      render 'edit'


    end
  end

  def destroy
    authorize @event, :destroy?
    if @event.destroy
      redirect_to  events_path, notice: "You have deleted event successfulle"




    end

  end


  private
  def event_params
    params.require(:event).permit(:title, :description, :start_date, :end_date, :venue, :location, :image, :category_id, :seats, :tag_list)


  end

  def set_event
    @event = Event.friendly.find(params[:id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] ="The page you requested does not exists"
    redirect_to events_path

  end



end
