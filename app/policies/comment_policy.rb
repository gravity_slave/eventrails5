class CommentPolicy < ApplicationPolicy
  def create?
    true if user.present?
  end
end