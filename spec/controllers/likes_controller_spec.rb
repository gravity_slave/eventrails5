require 'rails_helper'

RSpec.describe LikesController, type: :controller do
  before do
    @user = FactoryGirl.create(:user)
    @event = FactoryGirl.create(:event, user_id: @user.id)
    @like = FactoryGirl.attributes_for(:like, user: @user)
    @dislike = FactoryGirl.attributes_for(:dislike, user: @user)

    sign_in @user
  end

  it "shoild create like" do

    expect{
      post :create, event_id: @event.id, like: @like
    }.to change(Like, :count).by(1)
  end
  it "permitted for user to have 1 like per event" do
    post :create, event_id: @event.id, like: @like
    expect{
      post :create, event_id: @event.id, like: @dislike
    }.to change(Like, :count).by(0)
    expect(flash[:alert]).to match("You have already made your choice")


  end

end
