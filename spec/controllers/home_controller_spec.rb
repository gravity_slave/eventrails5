require 'rails_helper'

RSpec.describe HomeController, type: :controller do
 before { get :index}
  it { expect have_http_status :success}
  it { expect render_template "index"}
end
