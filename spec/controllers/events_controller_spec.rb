require 'rails_helper'

RSpec.describe EventsController, type: :controller do

  before do
    @title,@venue, @location = Faker::Name.name, Faker::Address.street_address, Faker::Address.state
    @user  = FactoryGirl.create(:user)
    @cat = FactoryGirl.create(:category)
    @event= FactoryGirl.create(:event, user_id: @user.id, category_id: @cat)

  end
  describe "INDEX" do
before {   get :index }
    it { expect render_template :index}
  end


  describe "Show" do

  before do
    get :show, id: @event

  end
 it { expect render_template :show}

  end
describe "ERROR CAUSE OF INVALLID ID" do
    before { get :show, id: 1337 }
  it { expect rescue_from ActiveRecord::RecordNotFound}
  it {expect set_flash}
end

  describe "EDIT" do
    before { get :edit, id: @event }
    it { expect render_template :edit}

  end

  describe "UPDATE" do
it "shoudl update with valid credentials" do
  sign_in @user
  patch :update, id: @event, event: { title: @title, location: @location, venue: @venue}

  expect(response).to redirect_to(@event)
  expect(assigns(:event).title).to eq(@title)
  expect(assigns(:event).venue).to eq(@venue)
  expect(assigns(:event).location).to eq(@location)
end

it "should not  update with invalid credentials" do
  sign_in @user
  patch :update, id: @event, event: { title: ""}
  expect(response).to render_template(:edit)
end
  end

  describe "NEW" do
    it 'should render new' do
      sign_in @user
      get :new

      expect(response).to render_template :new
    end
  end

  describe "CREATE" do
    it "should create with valid creadentials" do
      sign_in @user
      expect{
       post :create, event: { title: @title,start_date: DateTime.now, end_date: DateTime.now + 1.day, location: @location,
                              venue: @venue, category_id: @cat.id}
     }.to change(Event, :count).by(1)
      expect(flash[:notice]).to match("Event created!")
      expect(assigns(:event).title).to eq(@title)
      expect(assigns(:event).venue).to eq(@venue)
      expect(assigns(:event).location).to eq(@location)

    end

    it "should not  create with invalid creadentials" do
      sign_in @user
      expect{
        post :create, event: { title: ""}
      }.to change(Event, :count).by(0)
      expect(response).to render_template :new
    end
  end

  describe "DELETE" do
    it "should delete properly" do
      @event = FactoryGirl.create(:event, title: "JUSTIN BIEBER", location: Faker::Address.state,
                                  venue: Faker::Address.street_address, user_id: @user.id, category_id: @cat)
      sign_in @user
      expect{
       delete :destroy, id: @event
     }.to change(Event, :count).by(-1)

    end
  end

end
