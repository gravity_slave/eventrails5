require 'rails_helper'

RSpec.describe Admin::ApplicationController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      @user_admin = FactoryGirl.create(:admin)
      sign_in(@user_admin)
      get :index
      expect(response).to have_http_status(:success)
    end


    it " cant be accessed by non-admin" do
      @user = FactoryGirl.create(:user)
      sign_in @user
      get :index
      expect(response).to have_http_status :redirect
      expect(page).to have_current_path root_path
    end
  end

end
