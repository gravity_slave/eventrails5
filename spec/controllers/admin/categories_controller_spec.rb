require 'rails_helper'

RSpec.describe Admin::CategoriesController, type: :controller do
before do
  @admin_usr  = FactoryGirl.create(:admin)
  sign_in @admin_usr
end

  it " should GET index" do
    get  :index
    expect(response).to render_template :index
  end

  it "should GET new" do
    get :new
    expect(response).to render_template :new
  end

  it "should POSt create" do
    @category = FactoryGirl.attributes_for(:category)
    expect{
    post :create, category: @category
    }.to change(Category, :count).by(1)
    expect(flash[:notice]).to match("Category has been created")
  end

  it "shoudl GET edit" do
    @category = FactoryGirl.create(:category)
    get :edit, id: @category.id
    expect(response).to render_template :edit
  end

  it "should PATCH edit" do
    @category = FactoryGirl.create(:category)
    patch :update, id: @category, category: { name: "Qwerty", summary: "zxczxczxc"}
    expect(response).to redirect_to admin_categories_path
    expect(assigns(:category).name).to eq("Qwerty")
    expect(assigns(:category).summary).to eq("zxczxczxc")
    expect(flash[:notice]).to match("Category has been updated")
  end
  it "should not PATCH edit" do
    @category = FactoryGirl.create(:category)
    patch :update, id: @category, category: { name: "", summary: ""}
    expect(response).to render_template :edit

    expect(flash[:alert]).to match("Category has not been updated")

  end

  it "should DELETE destory" do
    @category = FactoryGirl.create(:category)
    expect {
      delete :destroy, id: @category.id
    }.to change(Category, :count).by(-1)
    expect(flash[:alert]).to match("Category has been deleted")
  end

end
