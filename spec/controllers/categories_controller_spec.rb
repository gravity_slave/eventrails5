require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe "it should render show" do
    before do
    @category = FactoryGirl.create(:category)
    get :show , id: @category.id
    end

    it { expect render_template "show"}

  end


end
