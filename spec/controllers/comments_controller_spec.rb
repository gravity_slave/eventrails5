require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before do
    @user = FactoryGirl.create(:user)
    @event = FactoryGirl.create(:event, user_id: @user.id)
    @comment  = FactoryGirl.attributes_for(:comment)
  end
  it "should create comments"  do
  sign_in @user
    expect{
      post :create, event_id: @event.id, comment: @comment
    }.to change(Comment, :count).by(1)
  end
  it "should create comments"  do
    sign_in @user
    expect{
      post :create, event_id: @event.id, comment: { body: ""}
    }.to change(Comment, :count).by(0)
  end

end
