FactoryGirl.define do
  factory :like do
    event
    user
    like "like"
  end
  factory :dislike, class: Like do
    event
    user
    like "dislike"
  end
end
