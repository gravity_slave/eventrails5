FactoryGirl.define do
  factory :event, aliases: [:occasion, :case] do
    title  { Faker::Name.name }
    description  Faker::Hipster.paragraph(4, true)
    start_date DateTime.now
    end_date DateTime.now
    venue  { Faker::Address.street_address }
    location { Faker::Address.state }
     user
    category
  end

  factory :another_event, class: Event do
    title  { Faker::Pokemon.name }
    description  Faker::Hipster.paragraph(4, true)
    start_date DateTime.now
    end_date DateTime.now
    venue  { Faker::Address.street_address }
    location { Faker::Address.state }
      user
    category
  end
end
