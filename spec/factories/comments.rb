FactoryGirl.define do
  factory :comment do
    body "MyText"
    event

    association :author, factory: :user
  end
end
