FactoryGirl.define do
  factory :user do
    first_name {Faker::Cat.name}
    last_name { Faker::Name.last_name }
    username { Faker::Internet.user_name(7..7)}
    organization_name { Faker::Company.name }
    email { Faker::Internet.email }
    password "asdfasdf"
    password_confirmation "asdfasdf"
  end

  factory :admin, class: User do
    first_name {Faker::Cat.name}
    last_name { Faker::Name.last_name }
    username { Faker::Internet.user_name(7..7)}
    organization_name { Faker::Company.name }
    email { Faker::Internet.email }
    password "asdfasdf"
    password_confirmation "asdfasdf"
    admin true
  end
end
