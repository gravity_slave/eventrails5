FactoryGirl.define do
  factory :category do
    name   Faker::Book.author
    summary Faker::Hipster.paragraph(2,false,4)
  end
end
