require 'rails_helper'

RSpec.describe User, type: :model do
it { expect validate_presence_of :username}
it { expect validate_length_of(:username).is_at_least(6)}
it { expect have_many :events}
it  { expect have_many :comments}
  it { expect have_many :attendances}
  it { expect have_many :attended_events}
end
