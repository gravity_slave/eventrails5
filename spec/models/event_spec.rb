require 'rails_helper'

RSpec.describe Event, type: :model do
it { expect validate_presence_of :name}
it { expect belong_to :user}
it { expect belong_to :category}
it { expect have_many :attendances}
it { expect have_many :attendees}
it { expect belong_to :taggings}
it { expect belong_to :tags}


end
