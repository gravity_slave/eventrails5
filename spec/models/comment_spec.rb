require 'rails_helper'

RSpec.describe Comment, type: :model do
it { expect belong_to :user}
it { expect belong_to :author}
it { validate_presence_of :body}

end
