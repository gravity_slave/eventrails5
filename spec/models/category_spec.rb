require 'rails_helper'

RSpec.describe Category, type: :model do
it { expect validate_length_of(:name).is_at_least(5)}

  it { expect validate_presence_of :name}
  it { expect have_many :events}

end
