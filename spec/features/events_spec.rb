require 'rails_helper'

RSpec.feature "Events", type: :feature do

  before do
    @user = FactoryGirl.create(:user)
@category = FactoryGirl.create(:category)
    @event = FactoryGirl.create(:event, user_id: @user.id, category_id: @category.id)
    @case = FactoryGirl.create(:case, user_id: @user.id,category_id: @category.id )
  end
  context "READ" do
    it 'should dislplay events' do

      visit events_path

      expect(page).to have_content(@event.title)
      expect(page).to have_content(@case.title)
    end

    it 'should show event' do
      visit event_path(@event)
      expect(page).to have_current_path(event_path @event)
      expect(page).to have_content(@event.title)
      expect(page).to have_content(@event.description)
      expect(page).to have_content(@event.venue)
      expect(page).to have_content(@event.location)

    end

  end

  context "UPDATE" do

    it "shoudl update with valid credentials" do
      sign_in_as(@user)
      visit edit_event_path(@case)

      fill_in "Title", with: "Shreder"
      fill_in "Description", with: "KAVABANGA"
      click_on("Update Event")
      expect(page).to have_current_path(event_path @case)
      expect(page).to have_content("Shreder")
      expect(page).to have_content("KAVABANGA")
    end

    it "shoudl not update with valid credentials" do
      sign_in_as @user
      visit edit_event_path(@case)
      fill_in "Title", with: ""
      fill_in "Description", with: "KAVABANGA"
      fill_in "Venue", with: ""
      fill_in "Location", with: ""
      click_on("Update Event")
      expect(page).to have_css("div.form-group.required.event_title.has-error")
      expect(page).to have_css("div.form-group.required.event_venue.has-error")
      expect(page).to have_css("div.form-group.required.event_location.has-error")


    end

  end

  context "CREATE" do


    it "shoudl not update with valid credentials" do
      sign_in_as @user
      visit events_path
      click_link("New Event")
      fill_in "Title", with: ""
      fill_in "Description", with: "KAVABANGA"
      fill_in "Venue", with: ""
      fill_in "Location", with: ""
      click_on("Create Event")
      expect(page).to have_css("div.form-group.required.event_title.has-error")
      expect(page).to have_css("div.form-group.required.event_venue.has-error")
      expect(page).to have_css("div.form-group.required.event_location.has-error")



    end

    it "should create with valid credentials" do
      sign_in_as @user
      visit new_event_url

      fill_in "Title", with: "My Title"
      fill_in "Venue", with: "My Venue"
      fill_in "Location", with: "My location"
      fill_in "Description", with: Faker::Lorem.paragraph(1, false)
      click_on("Create Event")
      expect(page).to have_current_path(event_path(Event.last))
      expect(page).to have_content("My Title")
      expect(page).to have_content("My Venue")
      expect(page).to have_content("My location")

    end

  end


  context "DESTROY" do
    it "should dlete " do
      sign_in_as @user
    visit event_path(@case)
    click_link("Delete Event")
    #It were 2 events before - look over before section
    expect(Event.count).to eq(1)
      expect(page).to have_current_path(events_path)
  end
  end

  context "NONE OWNER " do
    before do
      @user2 = FactoryGirl.create(:user)
      sign_in_as @user2
      visit event_path @event

    end

 it " can't edit" do

   expect(page).to_not have_content ('Edit Event')
 end

    it "can't destry" do
      expect(page).to_not have_content('Delete Event')

    end

    end

end

