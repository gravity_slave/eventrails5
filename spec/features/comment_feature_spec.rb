require 'rails_helper'
RSpec.describe "Comment posting", :type => :feature do
before do
  @user =FactoryGirl.create(:user)
  @event = FactoryGirl.create(:event, user_id: @user.id)
  sign_in_as(@user)
end

  it "should post" do
    visit event_path @event
    fill_in "comment[body]", with: "justin bieber"
    click_on("Create Comment")
    expect(page).to have_content("justin bieber")
  end
end
