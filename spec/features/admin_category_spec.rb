require 'rails_helper'

RSpec.describe " category features", :type => :feature do


    before do
      @user =  FactoryGirl.create(:user)
      @cat1 =  FactoryGirl.create(:category)
      @cat2 = FactoryGirl.create(:category, name: "Another name")
      @event1 = FactoryGirl.create(:event, user_id: @user.id, category_id: @cat1.id)
      @event2 = FactoryGirl.create(:event, user_id: @user.id, category_id: @cat1.id)
    @admin = FactoryGirl.create(:admin)
    sign_in_as @admin

  end

 it "should display index content" do
   visit admin_root_path

   expect(page).to have_content(User.count)
   expect(page).to have_content(Event.count)
   expect(page).to have_content(Category.count)

 end

  it "should  list users" do
    visit admin_root_path
    click_on("Users")
    expect(page).to have_content(@user.username)
    expect(page).to have_content(@admin.username)

  end

    it "should  list categories" do
      visit admin_root_path
      click_on("Categories")
      expect(page).to have_content(@cat1.name)
      expect(page).to have_content(@cat2.name)

    end


    it "should  list events" do
      visit admin_root_path
      click_on("Events")
      expect(page).to have_current_path events_path
      expect(page).to have_content(@event1.title)
      expect(page).to have_content(@event2.title)
      expect(page).to have_link(@event1.title,category_path(@event1))
      expect(page).to have_link(@event2.title,category_path(@event2))

    end



end